<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'rays');

/** MySQL database username */
define('DB_USER', 'rays');

/** MySQL database password */
define('DB_PASSWORD', 'P@ssw0rd1');

/** MySQL hostname */
define('DB_HOST', 'rays.db.11234044.hostedresource.com');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'l#8|5az,?^y!j^F)s{w[HLI?N+U)VD!XLos?bj8*AHbOl:- 3Emu-8GGc${F-`mj');
define('SECURE_AUTH_KEY',  'l?;/U2RwL~,2(%ZGb(ur*6%H~w5q&m;b0tigk8;Wv|~?~;>8DzHYP@aDiR:o2i@N');
define('LOGGED_IN_KEY',    '4g7~-Tx1!ec2yBH1rp!oH3yJS1aJg#n?J![Pmg-|k;F)4-ELx=kYCu},MjJ$oamI');
define('NONCE_KEY',        '@iQH(T7TI9F<i5p2:wUR@j%|`e@>+$-|+:Cu#WUNUgj ~X_{!KqTb*MF;_VKIim`');
define('AUTH_SALT',        '+Qt5_+_;evK E86:w[WFC-PHSpB8:@u+u-50>@{OCA+FNn>Z$--=&ZSyi#_TuRy-');
define('SECURE_AUTH_SALT', '64<u=4:#{9md|JW96>bw-! (J)#]I9ESh0q8<Bfkwotm:A[<`4+2l}GU-PPN]jHK');
define('LOGGED_IN_SALT',   '.oC5`xn,B;IS[E@h>)PS&_9`@Y-HD;4c8j.HZSupS)+n)Gfyc}@QHUv3kAPj4MMt');
define('NONCE_SALT',       '=u>$(FkZQ?&t+J^#OeYVHD`4xkFF_@JTvuduwepKn$;-(dN;>K ,?o -u 0,QQq5');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');