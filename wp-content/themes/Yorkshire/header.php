<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package web2feel
 * @since web2feel 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'web2feel' ), max( $paged, $page ) );

	?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link href='http://fonts.googleapis.com/css?family=Lato:400,300,700,900' rel='stylesheet' type='text/css'>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="page" class="hfeed site container_6">
	

	<div class="topnavi clearfix">
		<div class="container_6">
					<?php wp_nav_menu( array( 'container_id' => 'submenu', 'theme_location' => 'primary','menu_id'=>'web2feel' ,'menu_class'=>'sf-menu','fallback_cb'=> 'fallbackmenu' ) ); ?>

		</div>

	</div>

	<header id="masthead" class="site-header" role="banner">
			<div class="container_6 top clearfix">
				<div class="logo">
					<h1 class="site-title"><a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
					<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
<a href="http://www.facebook.com/RAYSseattle"><img src="/wp-content/uploads/2013/05/fb-icon.png" onmouseover="this.src='/wp-content/uploads/2013/05/fb-icon_h.png'" onmouseout="this.src='/wp-content/uploads/2013/05/fb-icon.png'" style="margin-top:20px; margin-left:5px;"></a>
<a href="http://twitter.com/RAYSseattle"><img src="/wp-content/uploads/2013/05/twitter-icon.png" onmouseover="this.src='/wp-content/uploads/2013/05/twitter-icon_h.png'" onmouseout="this.src='/wp-content/uploads/2013/05/twitter-icon.png'" style="margin-top:20px; margin-left:5px;"></a>
<a href="mailto:info@raysummit.com" target="_blank"><img src="/wp-content/uploads/2013/05/mail.png" onmouseover="this.src='/wp-content/uploads/2013/05/mail_h.png'" onmouseout="this.src='/wp-content/uploads/2013/05/mail.png'" style="margin-top:20px; margin-left:5px;"></a>

				</div>
<div style="float:right; margin-right:50px;">
Language/Язык:
<a href="http://raysummit.com/?lang=ru" hreflang="ru" title="Russian">
<img src="/wp-content/uploads/2013/05/russian-icon_small.png"></a>
<a href="http://raysummit.com/" hreflang="en" title="English">
<img src="/wp-content/uploads/2013/05/flag-us_small.png"></a>
				</div>
				
			</div>	
	</header><!-- #masthead .site-header -->
	

	
	<div id="main" class="site-main cf">